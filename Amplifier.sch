EESchema Schematic File Version 4
LIBS:RPi_Zero_pHat_Template-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wisdom:Hall_Effect_Sensor U4
U 1 1 60BB3B8E
P 2400 2100
F 0 "U4" H 2022 1796 50  0000 R CNN
F 1 "Hall_Effect_Sensor" H 2022 1705 50  0000 R CNN
F 2 "Sensor_Current:Allegro_SIP-3" H 1650 1700 50  0001 C CNN
F 3 "" H 1650 1700 50  0001 C CNN
	1    2400 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60BB3C03
P 4300 2500
F 0 "D3" H 4291 2716 50  0000 C CNN
F 1 "LED" H 4291 2625 50  0000 C CNN
F 2 "LED_THT:LED_D8.0mm" H 4300 2500 50  0001 C CNN
F 3 "~" H 4300 2500 50  0001 C CNN
	1    4300 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R8
U 1 1 60BB3C71
P 4700 3200
F 0 "R8" H 4768 3246 50  0000 L CNN
F 1 "220" H 4768 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 4700 3200 50  0001 C CNN
F 3 "~" H 4700 3200 50  0001 C CNN
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Dual_Generic U2
U 2 1 60BB3CF7
P 5400 4000
F 0 "U2" H 5400 4367 50  0000 C CNN
F 1 "Opamp_Dual_Generic" H 5400 4276 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5400 4000 50  0001 C CNN
F 3 "~" H 5400 4000 50  0001 C CNN
	2    5400 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R9
U 1 1 60BB3D56
P 6100 4450
F 0 "R9" H 6168 4496 50  0000 L CNN
F 1 "5.2" H 6168 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6100 4450 50  0001 C CNN
F 3 "~" H 6100 4450 50  0001 C CNN
	1    6100 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R10
U 1 1 60BB3DBA
P 6100 5100
F 0 "R10" H 6168 5146 50  0000 L CNN
F 1 "1K" H 6168 5055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6100 5100 50  0001 C CNN
F 3 "~" H 6100 5100 50  0001 C CNN
	1    6100 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2100 2400 1600
Wire Wire Line
	2850 2450 4150 2450
Wire Wire Line
	4150 2450 4150 2500
Wire Wire Line
	4450 2500 4700 2500
Wire Wire Line
	4700 2500 4700 3100
Wire Wire Line
	4700 3300 4700 3900
Wire Wire Line
	4700 3900 5100 3900
Wire Wire Line
	5700 4000 6100 4000
Wire Wire Line
	6100 4000 6100 4350
Wire Wire Line
	6100 4550 6100 4750
Wire Wire Line
	6100 5200 6100 5350
Wire Wire Line
	2400 2800 2400 5350
Wire Wire Line
	2400 5350 6100 5350
Connection ~ 6100 5350
Wire Wire Line
	6100 5350 6100 5650
Text Label 6100 5650 0    50   ~ 0
GND
Text Label 2400 1600 0    50   ~ 0
GPIO18
Wire Wire Line
	5100 4100 4750 4100
Wire Wire Line
	4750 4750 6100 4750
Wire Wire Line
	4750 4100 4750 4750
Connection ~ 6100 4750
Wire Wire Line
	6100 4750 6100 5000
$EndSCHEMATC
